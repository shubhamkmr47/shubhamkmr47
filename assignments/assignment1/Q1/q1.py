import datetime

master_dictionary={2:("Shubham","Bhopal","104/07/1995"),6:("Aekansh","Nagpur","22/11/1995"),9:("Priyansh","Jaipur","7/9/1995"),0:("Tamojit","Kolkata","8/01/1994"),8:("Piya","Raipur","23/07/1995"),5:("Shreeansh","Delhi","13/4/1995")}

acquaintances={2:[6,9,0,8],6:[2,0],9:[2],0:[2],8:[2]}

messages={2:[],6:[],9:[],0:[],9:[]}

bday_msg=[]

#linear search through dicitionary if user exists reurn 1
msg_count=0
def check_valid_user(n):
	for i in master_dictionary:
		if i==n:
			return 1
	else: 
		return 0

#if both exists add them in friends list
def add_friend(n1,n2):
	if (check_valid_user(n1)==1 and check_valid_user(n2)==1):		
		acquaintances[n1].append(n2)
		return True
	else:
		return False

#check for all friends
def add_friends(n1,n2):
	flag=0
	for i in n2:
		if (check_valid_user(n1)==1 and check_valid_user(i)==1):
			acquaintances[n1].append(i)
			flag=1
	if flag==1:
		return True	
	else:
		return False

#check for users_id and remove if present
def remove_user(n): 
	if check_valid_user==0:
		return False
	else:
		x=master_dictionary.pop(n)
		return True

def get_friends(n):
	if check_valid_user(n)==1:
		return tuple(acquaintances[n])
	else:
		return None


def friends_of_friends(n):
	x=get_friends(n)
	y=[]
	for i in x:
		z=list(get_friends(i))
		for j in z:
			flag=0
			for p in y:
				if p==j:
					flag=1
					break					
			if j!=n and flag==0:
				y.append(j)
	return tuple(y)

def send_message(n1,n2,n3):
	if check_valid_user(n1)!=1 or check_valid_user(n2)!=1:
		return False
	dt=datetime.datetime.now()
	date="%s/%s/%s" %(dt.day,dt.month,dt.year)
	time="%s:%s:%s" %(dt.hour,dt.minute,dt.second)
	msg=(n1,msg_count,n3,date,time)
	msg_count=msg_count+1	
	messages[n2].append(msg)
	for i in master_dictionary:
		x=(master_dictionary[i][2])
		a=x.split("/")
		a1=int(a[0])
		a2=int(a[1])
		if a1==dt.day and a2==dt.month:
			bday_msg.append((msg,n2))			
	return True		

def send_group_message(n1,n2,n3):
	for i in n2:
		send_message(n1,i,n3)
	return

def get_messages_from_friend(n1,n2):
	if check_valid_user(n1)!=1 or check_valid_user(n2)!=1:
		return None	
	x=[]
	for i in messages:
		for j in messages[i]:
			if j[0]==n2:
				x.append(j)
	return tuple(x)

def get_messages_from_all_friends(n):
	if check_valid_user(n)==1:
		return tuple(messages[n])
	else:
		return None

def get_birthday_messages(n):
	if check_valid_user(n)!=1:
		return None	
	x=[]
	for i in bday_msg:
		if i[1]==n:
			x.append(i[0])		
	return tuple(x)


def delete_message(n1,n2):
	if check_valid_user(n1)!=1:
		return False
	for i in messages[n1]:
		if i[1]==n2:
			messages[n1].remove(i)


def delete_messages(n1,n2):
	if check_valid_user(n1)!=1:
		return False
		del_count=0
	for i in n2:
		if i<msg_count:
			del_count=del_count+1 	
			delete_message(n1,i)
	return del_count

#check for all the messages and remove them if present
def delete_all_messages(n):
	if check_valid_user(n)!=1:
		return False
	for i in messages[n]:
		messages[n].remove(i)
	return

#if user exists remove
def delete_messaged_from_friends(n1,n2):
	if check_valid_user(n1)!=1:
		return False
	for i in messages[n1]:
		if i[0]==n2:
			messages[n1].remove(i)
	return

