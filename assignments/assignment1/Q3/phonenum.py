#"""Phonenum - an abstraction to represent mobile phone numbers."""

#"This type is meant to be used only in the classroom!"

__all__ = ["Phonenum", "phonenum_work", "phonenum_home", "phonenum_same"]

_phonenum_types_ = ["work", "home"]


#checks if the string has got digits or not and it has got digits or not else if it has more digits checks the next two digits are integer or not
#country code is assumed to be from 10 to 99
def Phonenum(numstr, type):
#	"""constructs an instance of a phone-number."""
	num = numstr.strip()	
	if len(num) < 10:
		return None
	if len(num) >10:
	   	if num[0].isdigit()==True and num[1].isdigit()==True and len(num) = 12:
			return (num,type.strip().lower())
	return (num, type.strip().lower())

def phonenum_work(tn):
	num, type = tn
	return type == "work"

def phonenum_home(tn):
	num, type = tn
	return type == "home"

def phonenum_same(this, that):
	return _phonenum_valid_(this) and _phonenum_valid_(that) and this[0] == that[0]

def _phonenum_valid_(tn):
	return isinstance(tn, tuple) and len(tn) == 2 and \
	         isinstance(tn[0], str) and len(tn[0]) == 10
