from phonenum import *

#checks whether the string hold digits
def is_valid_contact_number(n):
	if n.isdigit() is True:
		return 1
	else:
		return 0

def create_contacts():
	return {}

def add_contacts(contacts, name, phone_num, phone_type):
	k=0
	numb=' '	

	if is_valid_contact_number(phone_num)==1:	    	
		if contacts.has_key(name):
			phone_nums = contacts[name]
			phone_nums.append(Phonenum(phone_num, phone_type))
		else:
			contacts[name] = [ Phonenum(phone_num, phone_type) ]
	else:
		return None

def update_contact_number(contact_name, old_number, new_number):
	if is_valid_contact_number(old_number)!=1 or is_valid_contact_number(new_number)!=1:
		return False
	else:
		tv=contacts[contact_name]
		if phonenum_work(tv)==True:
			phonenum_type='work'
		else:
			phonenum_type='home'
			tx=(new_number,phonenum_type)
	  		contacts.update({contact_name:tx})
			return True

