from contacts import create_contacts
from contacts import add_contacts
from phonenum import *
	

def test_add_multiple_contact_numbers():
	c = create_contacts()
	add_contacts(c, 'Amy', "1837462849", "home")
	add_contacts(c, 'Amy', "2649827645", "work")
	add_contacts(c, 'Amy', "8365928847", "work")
	assert(len(c.keys()) == 1)

def test_generic_contacts_behavior():
	contacts = {}
	contacts["Aekansh"] = Phonenum("8475920572", "home")
	contacts["Piya"] = Phonenum("8452857265", "work")
	contacts["Rony"] = Phonenum("8475928472", "work")
	contacts["Sara"] = Phonenum("9977487", "home")
	assert(len(contacts.keys()) == 4)

	phone_Sara = contacts["Sara"] 
	assert(phone_Sara is None)

	try:
	phone_ken = contacts["Amy"]
		assert(0)
	except KeyError as e:
		assert(1)


if __name__ == '__main__':
	test_generic_contacts_behavior()
	test_add_multiple_contact_numbers()
