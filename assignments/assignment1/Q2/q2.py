#import the math inorder to use square root function
import math

#lenght is found using usual formula of distance
def length(p):
	return math.pow(math.pow(p[1][0]-p[0][0],2.0)+math.pow(p[1][1]-p[0][1],2.0)+math.pow(p[1][2]-p[0][2],2.0),0.5)

#normalization is vector divide by the modulus that is also the lenght
def normalize(p):
	return ((0,0,0),((p[1][0]-p[0][0])/length(p),(p[1][1]-p[0][1])/length(p),(p[1][2]-p[0][2])/length(p)))

def dot_product(p1,p2):
	return (((p1[1][0]-p1[0][0])*(p2[1][0]-p2[0][0]))+((p1[1][1]-p1[0][1])*(p2[1][1]-p2[0][1]))+((p1[1][2]-p1[0][2])*(p2[1][2]-p2[0][2])))

def cross_product(p1,p2):
	return ((0,0,0),(((p1[1][1]-p1[0][1])*(p2[1][2]-p2[0][2])-(p2[1][1]-p2[0][1])*(p1[1][2]-p1[0][2])),((p2[1][0]-p2[0][0])*(p1[1][2]-p1[0][2])-(p1[1][0]-p1[0][0])*(p2[1][2]-p2[0][2])),((p1[1][0]-p1[0][0])*(p2[1][1]-p2[0][1])-(p2[1][0]-p2[0][0])*(p1[1][1]-p1[0][1]))))

