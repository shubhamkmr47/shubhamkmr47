__all__ = ["Phonenum", "phonenum_work", "phonenum_home", "phonenum_same"]

_phonenum_types_ = ["work", "home"]

#country code is assumed to be 100 to 200

def Phonenum(numstr, type):
	num = numstr.strip()
	if (len(num) is not 10 and (len(num) is not 13 and len(num/10**10)<100 and len(num/10**10)>150)) and num.isdigit()=="False":
		return None
	return (num, type.strip().lower())

def phonenum_work(tn):
	num, type = tn
	return type == "work"

def phonenum_home(tn):
	num, type = tn
	return type == "home"

def phonenum_same(this, that):
	return _phonenum_valid_(this) and _phonenum_valid_(that) and this[0] == that[0]

def _phonenum_valid_(tn):
	return isinstance(tn, tuple) and len(tn) == 2 and isinstance(tn[0], str) and len(tn[0]) == 10

#""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""#

from phonenum import *

def create_contacts():
	return {}

def add_contacts(contacts, name, phone_num, phone_type):
	if contacts.has_key(name):
		phone_nums = contacts[name]
		phone_nums.append(Phonenum(phone_num, phone_type))
	else:
		contacts[name] = [ Phonenum(phone_num, phone_type) ]

def update_contact_number(contacts, contact_name, old_number, new_number):
	phone_num = contacts[contact_name]
	for num in phone_num:
		if num==old_number:
			num=new_number
			return True
	return False
