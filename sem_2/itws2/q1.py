#users dictionary
#Users={ 1:("Rony","Kolkata","6/12/1995"), 2:("Aekansh","Nagpur","21/09/1994"), 3:("Piya","Mumbai","04/12/1995")}

#Acquaintances
#Acquaintances={1:[2,3], 2:[3], 3:[1,2]}

#checks whether user id is present in acq. and the friend already exist in the corresponding key
def add_friend(user_id, friend_id):
	if Acquaintances.has_key(user_id)==True:
		Acquaintances[user_id].append(friend_id)
		return True
	else:
	 	return False

#assuming f_list is the list of friends id also checks if friend is already present in the list
def add_friends(user_id, (friend_id_1, ..., friend_id_n)):
	for i in Acquaintances[user_id]:
		if Acquaintances.has_key(i)==False
			Acquaintances[user_id].append(friend_id)
			return True
		else:
	                 return False

def remove_user(user_id):
	if Acquaintances.has_key(user_id)==True:
		del Acquaintances[user_id]
		return True
	else:
		return False

def get_friends(user_id):
	if Acquaintances.has_key(user_id)==True:
		return Acquaintances[user_id]

def get_friends_of_friends(user_id):
	for i in Acquaintances[user_id]:
		return Acquaintances[i]

#checks if both sender_id and receiver_id are present in Acq.
def send_message(sender_id, receiver_id, msg):
	if Acquaintances.has_key(sender_id)==True and Acquaintances.has_key(receiver_id)==True:
		Messages[receiver_id].append(msg)
		return True
	else:
	 	return False

#if the user id exists checks for other receivers an append the messages 
def send_group_message(sender_id, (receiver_id_1, ..., receiver_id_n), msg):
	if Acquaintances.has_key(sender_id)==True:
		for i in Acquaintances[sender_id]:
			if Acquaintances.has_key(i)==True:
				Messages[i].append(msg)
			else:
				return False
			return True
	else:
		return False

def get_messages_from_friend(receiver_id, friend_id):
	if Acquaintances.has_key(sender_id)==True and Acquaintances.has_key(receiver_id)==True:
		t=()
	        for i in Acquaintances[receiver_id]:
	        	if i==friend_id:
	           		t=t+(i,)
	      	return t
	return None

def get_messages_from_all_friends(receiver_id):
	t=()
	for i in Messages(receiver_id):
		t=t+(i,)
	return t

#functon that checks the message received dates and sending a tuple of messages
def get_birth_day_messages(receiver_id):
	t=()
	for i in Messages[receiver_id]:
		if i[3]==Users[receiver_id][2]:
			t=t+(i,)
	return t

#deleting the messages of user id of given message id
def delete_message(user_id, msg_id):
	if Messages.has_key(user_id)==True:
		for i in Messages[user_id]:
			if i[1] == msg_id:
				i=i.remove[msg_id]
		return True
	return False

#number messages deleted is count and is initialized as 0
#every messages has got a specific message id so the message id is removd after every linear search
def delete_messages(user_id, msg_id):
	count=0
	for i in  Messages[user_id]:
		for j in msg_id:
			if j==i[1]:
				j=j.remove[msg_id]
				count=count+1
	return count

#count the number of messages in the corresponding user_id and deleting the complete list and then making it empty
def delete_all_messages(user_id):
	count = 0
	for i in Messages[user_id]:
		count=count+1
	del Messages[user_id]
	Messages[user_id]=[]
	return count

def delete_messaged_from_friend(receiver_id, friend_id):
	count =0
	for i in Messages[user_id]:
		if i[0]==receiver_id:
			i=i.remove[i]
		count=count+1
	return count
